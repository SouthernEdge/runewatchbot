const Discord = require("discord.js");

const mysql = require('mysql');
const client = new Discord.Client();
// const connection = require("./dbconf.js");
const connection = mysql.createConnection({
    host: '51.38.184.171',
    user: 'root',
    password: '3bHLxgxH8QynMHmI9g8C',
    database: 'runewatch'
});

connection.connect((err) => {
    if (err) throw err;
});

client.on("ready", () => {
    client.user.setActivity('runewatch.com');
    console.log("Bot Initialized!");
});

const staffNotificationChannel = '668138736164536326';
const liveFeedNotificationChannel = '306476867370745856';

const minutes = 1, heartbeat = minutes * 60 * 1000;
const bot = client;

var sendSiteNotification = function(messageBody) {
    const embed = new Discord.RichEmbed()
        .setTitle('Website Notification')
        .setAuthor('System')
        .addField('Message: ', messageBody.message, true)
        .setColor(16776960)
        .setTimestamp();
    bot.channels.get(staffNotificationChannel).send({embed});
};

var sendLiveFeedNotification = function (type, messageBody) {
    var title = 'RSN Updated'
    if(type == 'published case') {
        title = 'New Case Published'
    }

    const embed = new Discord.RichEmbed()
    .setTitle(title)
    .setAuthor(messageBody.part_one)
    .addField('Rating', messageBody.part_two, true)
    .addField('Type', messageBody.part_three, true)
    .setColor(16776960)
    .setTimestamp();
    bot.channels.get(liveFeedNotificationChannel).send({embed});
};

setInterval(function () {
    console.log('running task');
    connection.query("SELECT * FROM runewatch.task_logs WHERE runewatch.task_logs.status= ?", 'open', function (err, rows, results) {
        if (err) throw err;

        if (rows.length > 0) {
            console.log(rows);

            var counter = rows.length-1;
            for (var i in rows) {

                connection.query('UPDATE runewatch.task_logs SET runewatch.task_logs.status=\'completed\' WHERE runewatch.task_logs.id = ?', rows[i].id, function (error, row, result) {
                    if (row.affectedRows > 0) {
                    } else {
                        bot.channels.get(staffNotificationChannel).send('Error with case status task');
                    }
                });

                var messageBody = JSON.parse(rows[i].payload);

                var type = rows[i].type;

                if(type == 'schedule' || type == 'user' || type == 'system') {
                    sendSiteNotification(messageBody);
                }

                if(type == 'published case') {
                    sendLiveFeedNotification(type, messageBody);
                }

                counter--;
            }
        }
    });

    console.log('finished task');
}, heartbeat);

client.login("NTg5NDg1MzU2NTQ2NjU0MjE5.XQUXBQ.AGfh4vcOSmtywLIGrxSDVFZR-98");